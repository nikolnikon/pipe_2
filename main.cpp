#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
    mkfifo("/home/box/in.fifo", 0666);
    mkfifo("/home/box/out.fifo", 0666);

    int fd_in;
    int fd_out;
    char buf[1024];

    if ((fd_in = open("/home/box/in.fifo", O_RDONLY | O_NONBLOCK)) < 0)
        return 1;

    if ((fd_out = open("/home/box/out.fifo", O_WRONLY)) < 0)
        return 1;
    
    int rr;
    int wr;
    while(1) {
        if ((rr = read(fd_in, buf, 1024)) <= 0) {
            close(fd_in);
            close(fd_out);
            return rr;
        }

        if ((wr = write(fd_out, buf, rr)) <= 0) {
            close(fd_in);
            close(fd_out);
            return wr;
        }

    }
    
    return 0;
}
